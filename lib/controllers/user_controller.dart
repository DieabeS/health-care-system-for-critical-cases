import 'package:get/get.dart';
import 'package:senior/model/edit_profile_model.dart';
import 'package:senior/network/api.dart';

class UserController extends GetxController {
  final id = ''.obs;
  final userName = ''.obs;
  final firstName = ''.obs;
  final lastName = ''.obs;
  final address = ''.obs;
  final dateofbirth = ''.obs;
  final gender = ''.obs;
  final phoneNumber = ''.obs;
  final isLoading = true.obs;
  final editProfileRequest = EditProfileRequest().obs;

  void getProfile() async {
        isLoading.value=true;

    try {
      Api api = new Api();
      await api.infonote().then((value) {
        userName(value.userName);
        firstName(value.firstname);
        dateofbirth(value.dateofbirth);
        lastName(value.lastname);
        address(value.address);
        phoneNumber(value.phonenumber);
        gender(value.gender);

        print(value.firstname);
        print(value.lastname);

        print(value.phonenumber);

        print(value.userName);
        
      });
    } finally {
      isLoading(false);
    }
  }

  void editProfile() async {
    Api api = new Api();
    editProfileRequest.value.id=id.value;
    await api.editProfile(editProfileRequest.value);
  }
}
