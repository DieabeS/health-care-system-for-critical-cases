class EditProfileRequest {
  String id;
  String firstName;
  String lastName;
  String dateOfBirth;
  String gender;
  String phoneNumber;
  String address;

  EditProfileRequest(
      {this.id,
      this.firstName,
      this.lastName,
      this.dateOfBirth,
      this.gender,
      this.phoneNumber,
      this.address});

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      '_id': id.trim(),
      'FirstName': firstName.trim(),
      'LastName': lastName.trim(),
      // 'Date_of_Birth': dateOfBirth.trim(),
      'Gender': gender.trim(),
      'PhoneNumber': phoneNumber.trim(),
      'Address': address.trim()
    };
    return map;
  }
}
