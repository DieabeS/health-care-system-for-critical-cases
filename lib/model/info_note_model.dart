


class InfoNoteModel
{
  String userName;
String firstname;
  String lastname;
  String address;
  String dateofbirth;
  String gender;
  String phonenumber;


InfoNoteModel
({this.firstname,this.lastname,this.address,this.dateofbirth,this.gender,this.phonenumber,this.userName});



factory InfoNoteModel.fromJson(Map<String, dynamic >json)
{
return InfoNoteModel (
  userName: json['user']['username'],
  firstname :  json['user']['FirstName'],
  lastname : json['user']['LastName'],
    address : json['user']['Address'],
    dateofbirth  :json['user']['Date_of_Birth'],
    gender : json['user']['Gender'],
    phonenumber : json['user']['PhoneNumber'].toString()
);
}

}