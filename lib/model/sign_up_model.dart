class SignUpModel
{
  String id;

  SignUpModel({
this.id

  });

  
factory SignUpModel.fromJson(Map<String ,dynamic>json){
  return SignUpModel(id: json["id"]);
}

}


class SignUpRequest
{
  String username;
  String email;
  String password;


  SignUpRequest({

this.email,this.password,this.username
  });

  Map<String,dynamic> toJson()
{
  Map<String,dynamic> map ={
'email' : email.trim(),
'username':username.trim(),
'password' : password.trim(),
  };
  return map;
}
}