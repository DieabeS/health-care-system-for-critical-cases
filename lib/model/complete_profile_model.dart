
class CompleteProfileModel
{
String status;


CompleteProfileModel({
  this.status
});


factory CompleteProfileModel.fromJson(Map<String ,dynamic>json){
  return CompleteProfileModel(status: json["status"]);
}
}

class CompleteProfileRequest
{
String userType;
String firstName;
String lastName;
String dateOfBirth;
String gender;
String phoneNumber;


CompleteProfileRequest({
  this.userType,this.firstName, this.lastName,this.dateOfBirth,this.gender,this.phoneNumber
});

Map<String,dynamic> toJson()
{
  Map<String,dynamic> map ={
'FirstName' : firstName.trim(),
'LastName' : lastName.trim(),
'Date_of_Birth':dateOfBirth.trim(),
'Gender': gender.trim(),
'PhoneNumber':phoneNumber.trim(),
'userType' :userType.trim()
  };
  return map;
}
}