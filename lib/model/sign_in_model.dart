class SignInModel {
  String token;
  String id;

  SignInModel({this.token, this.id});

  factory SignInModel.fromJson(Map<String, dynamic> json) {
    return SignInModel(token: json["token"], id: json['_id']);
  }
}

class SignInRequest {
  String username;
  String password;

  SignInRequest({this.password, this.username});

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      'username': username.trim(),
      'password': password.trim(),
    };
    return map;
  }
}
