import 'package:flutter/material.dart';
import 'package:senior/screens/sign_in/sign_in_screen.dart';
import 'package:senior/shared/routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SignInScreen(),
      routes: routes,
      theme: ThemeData(
          primaryColor: Colors.blueGrey[700],
          scaffoldBackgroundColor: Colors.white),
    );
  }
}
