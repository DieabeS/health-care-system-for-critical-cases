import 'package:flutter/material.dart';
import 'package:senior/screens/complete_profile/complete_profile_screen.dart';
import 'package:senior/screens/edit_profile/edit_profile_screen.dart';
import 'package:senior/screens/forgot_password/forgot_password_screen.dart';
import 'package:senior/screens/home/home_screen.dart';
import 'package:senior/screens/main/main_screen.dart';
import 'package:senior/screens/profile/profile_screen.dart';
import 'package:senior/screens/sign_in/sign_in_screen.dart';
import 'package:senior/screens/sign_up/sign_up_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SignInScreen.routeName: (context) => SignInScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  Home.routeName: (context) => Home(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
  EditProfileScreen.routeName: (_) => EditProfileScreen(),
  CompleteProfileScreen.routeName: (_) => CompleteProfileScreen(),
  MainScreen.routeName: (_) => MainScreen(),
};
