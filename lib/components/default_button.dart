
import 'package:flutter/material.dart';

class DefaultButton extends StatelessWidget {
  final String text;
  final Function press;
  const DefaultButton({
    Key key,@required this.text,@required this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector( 
                  child: Container(
          height: 55,
          width: 300,
          decoration: BoxDecoration(
              color: Colors.cyanAccent,
              borderRadius: BorderRadius.circular(100)),
          child: Center(child: Text(text))),
    onTap: press
    );
  }
}
