import 'package:flutter/material.dart';

import 'edit_profile_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          children: [
            EditProfileForm(),
          ],
        ),
      ),
    );
  }
}

