import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:senior/components/default_button.dart';
import 'package:senior/controllers/user_controller.dart';
import 'package:senior/screens/main/main_screen.dart';
import 'package:senior/screens/profile/profile_screen.dart';

class EditProfileForm extends StatefulWidget {
  const EditProfileForm({
    Key key,
  }) : super(key: key);

  @override
  _EditProfileFormState createState() => _EditProfileFormState();
}

String date;

class _EditProfileFormState extends State<EditProfileForm> {
  UserController controller = Get.find<UserController>();
  GlobalKey<FormState> globalKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Obx(() => Form(
          key: globalKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller:
                      TextEditingController(text: controller.firstName.value),
                  decoration: InputDecoration(
                      labelText: 'First Name',
                      prefixIcon: Icon(Icons.person_outline),
                      hintStyle: TextStyle(fontSize: 15)),
                  onSaved: (value) {
                    controller.editProfileRequest.value.firstName = value;
                  },
                ),
                SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller:
                      TextEditingController(text: controller.lastName.value),
                  decoration: InputDecoration(
                      labelText: 'Last Name',
                      prefixIcon: Icon(Icons.person_outline),
                      hintStyle: TextStyle(fontSize: 15)),
                  onSaved: (value) {
                    controller.editProfileRequest.value.lastName = value;
                  },
                ),
                SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller:
                      TextEditingController(text: controller.gender.value),
                  decoration: InputDecoration(
                      labelText: 'Gender',
                      prefixIcon: Icon(Icons.wc_outlined),
                      hintStyle: TextStyle(fontSize: 15)),
                  onSaved: (value) {
                    controller.editProfileRequest.value.gender = value;
                  },
                ),
                SizedBox(
                  height: 30,
                ),
                // TextFormField(
                //   readOnly: true,
                //   keyboardType: TextInputType.datetime,
                //   decoration: InputDecoration(
                //       prefixIcon: Icon(Icons.date_range_outlined),
                //       hintStyle: TextStyle(fontSize: 15)),
                //   onTap: () {
                //     showDatePicker(
                //             context: context,
                //             initialDate: DateTime.now(),
                //             firstDate: DateTime(1900),
                //             lastDate: DateTime(2022))
                //         .then((value) {
                //       setState(() {
                //         date = value.toString();
                //       });
                //     });
                //   },
                //   onSaved: (value) {
                //     controller.editProfileRequest.value.dateOfBirth = value;
                //   },
                //   controller: TextEditingController(
                //       text: date == null
                //           ? 'Date of Birth'
                //           : DateFormat()
                //               .add_yMd()
                //               .format(DateTime.parse(date))),
                // ),
                // SizedBox(
                //   height: 30,
                // ),
                TextFormField(
                  controller:
                      TextEditingController(text: controller.address.value),
                  decoration: InputDecoration(
                      labelText: 'Address',
                      prefixIcon: Icon(Icons.location_on_outlined),
                      hintStyle: TextStyle(fontSize: 15)),
                  onSaved: (value) {
                    controller.editProfileRequest.value.address = value;
                  },
                ),
                SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller:
                      TextEditingController(text: controller.phoneNumber.value),
                  decoration: InputDecoration(
                      labelText: 'Phone Number',
                      prefixIcon: Icon(Icons.phone_outlined),
                      hintStyle: TextStyle(fontSize: 15)),
                  onSaved: (value) {
                    controller.editProfileRequest.value.phoneNumber = value;
                  },
                ),
                SizedBox(
                  height: 30,
                ),
                DefaultButton(
                    text: 'Save',
                    press: () async {
                      if (globalKey.currentState.validate()) {
                        globalKey.currentState.save();
                        // ignore: await_only_futures
                        await controller.editProfile();
                        controller.getProfile();
                        Navigator.pop(context);
                      }
                      print(controller.editProfileRequest.value.id);

                      print(controller.editProfileRequest.value.firstName);
                      print(controller.editProfileRequest.value.lastName);

                      print(controller.editProfileRequest.value.address);

                      print(controller.editProfileRequest.value.gender);
                      print(controller.editProfileRequest.value.dateOfBirth);
                      print(controller.editProfileRequest.value.phoneNumber);
                    })
              ],
            ),
          ),
        ));
  }
}
