import 'package:flutter/material.dart';
import 'package:senior/screens/sign_up/components/sign_up_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 100,
          ),
          CircleAvatar(
            radius: 50,
            child: Text('HCS'),
            backgroundColor: Colors.teal,
            foregroundColor: Colors.white,
          ),
          SizedBox(
            height: 20,
          ),
          Text('Health Care System'),
          SizedBox(
                height: 50
              ),
          SignUpForm(),
        ],
      ),
    );
  }
}
