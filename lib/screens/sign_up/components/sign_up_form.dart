import 'package:flutter/material.dart';
import 'package:senior/components/default_button.dart';
import 'package:senior/model/sign_up_model.dart';
import 'package:senior/network/api.dart';
import 'package:senior/screens/complete_profile/complete_profile_screen.dart';
import 'package:senior/screens/sign_in/sign_in_screen.dart';

class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  SignUpRequest request;
  GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  TextEditingController _controller = new TextEditingController();
  @override
  void initState() {
    super.initState();
    request = new SignUpRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: globalKey,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          children: [
            TextFormField(
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.person_outline),
                  hintText: "Username",
                  hintStyle: TextStyle(fontSize: 15)),
              onSaved: (value) {
                request.username = value;
              },
            ),
            SizedBox(height: 20),
            TextFormField(
              decoration: InputDecoration(
                  hintText: 'Email',
                  prefixIcon: Icon(Icons.email_outlined),
                  hintStyle: TextStyle(fontSize: 15)),
              onSaved: (value) {
                request.email = value;
              },
            ),
            SizedBox(height: 20),
            TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                  hintText: 'Password',
                  prefixIcon: Icon(Icons.lock_outline),
                  hintStyle: TextStyle(fontSize: 15)),
              onSaved: (value) {
                request.password = value;
              },
            ),
            SizedBox(height: 20),
            TextFormField(
              controller: _controller,
              obscureText: true,
              decoration: InputDecoration(
                  hintText: 'Repeat Password',
                  prefixIcon: Icon(Icons.lock_outline),
                  hintStyle: TextStyle(fontSize: 15)),
            ),
            SizedBox(height: 35),
            DefaultButton(
                text: 'Create account',
                press: () async {
                  if (globalKey.currentState.validate()) {
                    globalKey.currentState.save();
                    print(request);
                    await Api().signup(request).then((value) {
                      if (value.id.isNotEmpty) {
                        Navigator.pushNamed(context, CompleteProfileScreen.routeName);
                        // print('success');
                      }
                    });
                  }
                }),
            SizedBox(height: 35),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Already have an account?",
                ),
                Padding(
                  padding: EdgeInsets.only(right: 5),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      SignInScreen.routeName,
                    );
                  },
                  child: Text(
                    "Sign in",
                    style: TextStyle(color: Colors.blue),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
