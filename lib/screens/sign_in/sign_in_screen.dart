import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:senior/controllers/user_controller.dart';
import 'components/body.dart';

class SignInScreen extends StatelessWidget {
  static String routeName = '/sign_in';
  final UserController controller = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.greenAccent,
        body: Container(
          height: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.blue, Colors.greenAccent.withOpacity(0.8)],
            ),
          ),
          child: Body(),
        ),
      ),
    );
  }
}
