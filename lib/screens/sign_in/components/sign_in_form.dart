import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:senior/components/default_button.dart';
import 'package:senior/controllers/user_controller.dart';
import 'package:senior/model/sign_in_model.dart';
import 'package:senior/network/api.dart';
import 'package:senior/screens/complete_profile/complete_profile_screen.dart';
import 'package:senior/screens/forgot_password/forgot_password_screen.dart';
import 'package:senior/screens/main/main_screen.dart';
import 'package:senior/screens/sign_up/sign_up_screen.dart';

class SignInForm extends StatefulWidget {
  @override
  _SignInFormState createState() => _SignInFormState();
}

class _SignInFormState extends State<SignInForm> {
  SignInRequest request;
  GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  UserController controller = Get.find<UserController>();
  @override
  void initState() {
    super.initState();
    request = new SignInRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: globalKey,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.person_outline),
                hintText: "Username",
                hintStyle: TextStyle(fontSize: 15)),
            onSaved: (value) {
              request.username = value;
            },
          ),
          SizedBox(height: 20),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(
                hintText: 'Password',
                prefixIcon: Icon(Icons.lock_outline),
                hintStyle: TextStyle(fontSize: 15)),
            onSaved: (value) {
              request.password = value;
            },
          ),
          SizedBox(height: 35),
          DefaultButton(
              text: 'Sign In',
              press: () async {
                if (globalKey.currentState.validate()) {
                  globalKey.currentState.save();
                  print(request.username);

                  await Api().logIn(request).then((value) {
                    if (value.token.isNotEmpty) {
                      controller.id(value.id) ;
                      print(controller.id.value);
                      Navigator.pushReplacementNamed(
                          context, MainScreen.routeName);
                    }
                  });
                }
              }),
          SizedBox(height: 35),
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(
                context,
                ForgotPasswordScreen.routeName,
              );
            },
            child: Text(
              "Forget Passowrd ?",
              style: TextStyle(fontSize: 18),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Don\'t have an account?',
                style: TextStyle(fontSize: 18),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushReplacementNamed(
                      context, SignUpScreen.routeName);
                },
                child: Text(
                  "Sign Up",
                  style: TextStyle(
                      color: Colors.cyanAccent,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          )
        ]),
      ),
    );
  }
}
