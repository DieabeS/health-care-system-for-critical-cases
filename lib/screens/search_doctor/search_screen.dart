import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:senior/network/api.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {


  String cardValue;
  List listItem = ['Doctors', 'Nurses', 'Hospitals',];
  String value;
List list;
  @override
  void initState() {
    super.initState();
    Api api =new Api();
 api.getResult().then((value) {
   list=value.list;
 });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Container(
            decoration: BoxDecoration(color: Colors.cyanAccent),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: [
                  SizedBox(height: 60),
                  Text(
                    "Search Doctor",
                    style: TextStyle(color: Colors.white, fontSize: 30),
                  ),
                  SvgPicture.asset(
                    'assets/icons/user-md-solid.svg',
                    color: Colors.white,
                    height: 40,
                  ),
                  Text("Search Doctor From Our Database",
                      style: TextStyle(fontSize: 20, color: Colors.white)),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    height: 60,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(25),
                      border: Border.all(
                        color: Color(0xFFE5E5E5),
                      ),
                    ),
                    child: DropdownButton(
                      isExpanded: true,
                      underline: SizedBox(),
                      icon: Icon(Icons.local_hospital),
                      hint: Text('Choose treatment'),
                      items: listItem.map((valueItem) {
                        return DropdownMenuItem(
                          value: valueItem,
                          child: Text(valueItem),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          value = newValue;
                        });
                      },
                      value: value,
                    ),
                  ),
                  Container(
                    child: TextField(
                      onChanged: (value) {},
                      style: TextStyle(),
                      textInputAction: TextInputAction.search,
                      onSubmitted: (value) {
                        cardValue = value;
                      },
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.search,
                        ),
                        hintText: 'Search'.toUpperCase(),
                      ),
                    ),
                  ),
                ],
              ),
            )),
        preferredSize: Size(double.infinity, 300),
      ),
      body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, i) => Card(
              child: ListTile(leading: Text('$list'),),
            ),
          )),
    );
  }
}

// static String routeName='/sign_in';
