import 'package:flutter/material.dart';
import 'package:senior/screens/complete_profile/components/body.dart';

class CompleteProfileScreen extends StatelessWidget {
  static String routeName='/complete_profile';
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.greenAccent,
      body: Container(
          height: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Colors.blue, Colors.greenAccent.withOpacity(0.8)])),
          child: Body()),
    ));
  }
}
