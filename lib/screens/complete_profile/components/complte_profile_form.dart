import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:senior/components/default_button.dart';
import 'package:senior/controllers/user_controller.dart';
import 'package:senior/model/complete_profile_model.dart';
import 'package:senior/network/api.dart';
import 'package:senior/screens/main/main_screen.dart';
import 'package:senior/screens/sign_in/sign_in_screen.dart';

class CompleteProfileForm extends StatefulWidget {
  @override
  _CompleteProfileFormState createState() => _CompleteProfileFormState();
}

class _CompleteProfileFormState extends State<CompleteProfileForm> {
  CompleteProfileRequest request;
  String date;
  GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  UserController controller = Get.find<UserController>();

  @override
  void initState() {
    super.initState();
    request = new CompleteProfileRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: globalKey,
      child: Padding(
        child: Column(children: [
          SizedBox(
            height: 30,
          ),
          Container(
            child: Row(
              children: [
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.person_outline,
                  color: Colors.grey[700],
                ),
                SizedBox(
                  width: 20,
                ),
                DropdownButton<String>(
                  underline: SizedBox(),
                  value: request.userType,
                  style: TextStyle(color: Colors.white),
                  iconEnabledColor: Colors.grey[700],
                  items: <String>[
                    'Doctor',
                    'Patient',
                    'Nurse',
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        style: TextStyle(color: Colors.grey[800]),
                      ),
                    );
                  }).toList(),
                  hint: Text(
                    "User Type",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                  ),
                  onChanged: (String value) {
                    setState(() {
                      request.userType = value;
                    });
                  },
                ),
              ],
            ),
          ),
          Divider(
            thickness: 1.75,
            color: Colors.grey[700],
          ),
          SizedBox(
            height: 30,
          ),
          TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.person_outline),
                hintText: "First Name",
                hintStyle: TextStyle(fontSize: 15)),
            onSaved: (value) {
              request.firstName = value;
            },
          ),
          SizedBox(
            height: 30,
          ),
          TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.person_outline),
                hintText: "Last Name",
                hintStyle: TextStyle(fontSize: 15)),
            onSaved: (value) => request.lastName = value,
          ),
          SizedBox(
            height: 30,
          ),
          TextFormField(
            readOnly: true,
            keyboardType: TextInputType.datetime,
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.date_range_outlined),
                hintStyle: TextStyle(fontSize: 15)),
            onTap: () {
              showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(1900),
                      lastDate: DateTime(2022))
                  .then((value) {
                setState(() {
                  date = value.toString();
                });
              });
            },
            onSaved: (value) {
              request.dateOfBirth = value;
            },
            controller: TextEditingController(
                text: date == null ? 'Date of Birth' : ''),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            child: Row(
              children: [
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.wc_outlined,
                  color: Colors.grey[700],
                ),
                SizedBox(
                  width: 20,
                ),
                DropdownButton<String>(
                  underline: SizedBox(),
                  value: request.gender,
                  style: TextStyle(color: Colors.white),
                  iconEnabledColor: Colors.grey[700],
                  items: <String>[
                    'Female',
                    'Male',
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        style: TextStyle(color: Colors.grey[800]),
                      ),
                    );
                  }).toList(),
                  hint: Text(
                    "Gender",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                  ),
                  onChanged: (String value) {
                    setState(() {
                      request.gender = value;
                    });
                  },
                ),
              ],
            ),
          ),
          Divider(
            thickness: 1.75,
            color: Colors.grey[700],
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.phone_android_outlined),
                hintText: "Phone Number",
                hintStyle: TextStyle(fontSize: 15)),
            onSaved: (value) {
              request.phoneNumber = value;
            },
          ),
          SizedBox(
            height: 30,
          ),
          DefaultButton(
            text: 'Continue',
            press: () async {
              if (globalKey.currentState.validate()) {
                globalKey.currentState.save();
                await Api().completeProfile(request).then(
                  (value) {
                    Navigator.pushNamedAndRemoveUntil(
                      context,
                      SignInScreen.routeName,(route)=>false
                    );
                    if (value.status == 'true') {}
                  },
                );
              }
            },
          )
        ]),
        padding: EdgeInsets.symmetric(horizontal: 40),
      ),
    );
  }
}
