import 'package:flutter/material.dart';
import 'package:senior/screens/complete_profile/components/complte_profile_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        SizedBox(height: 150),
        CompleteProfileForm(),
        SizedBox(height: 40),
    
      ],
    ));
  }
}
