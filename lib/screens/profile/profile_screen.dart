import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:senior/components/default_button.dart';
import 'package:senior/controllers/user_controller.dart';
import 'package:senior/screens/edit_profile/edit_profile_screen.dart';
import 'package:senior/screens/sign_in/sign_in_screen.dart';

import 'components/info.dart';

class ProfileScreen extends StatefulWidget {
  static String routeName = '/profile';

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final UserController controller = Get.find<UserController>();

  @override
  void initState() {
    super.initState();
    controller.getProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
        backgroundColor: Colors.cyanAccent,
        actions: [
          Row(
            children: [
              Text('Log Out'),
              IconButton(
                icon: Icon(Icons.logout),
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, SignInScreen.routeName, (route) => false);
                },
              ),
            ],
          ),
        ],
      ),
      body: Obx(
        () {
          if (controller.isLoading.value)
            return Center(child: CircularProgressIndicator());
          else
            return SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: [
                  SizedBox(height: 50),
                  Text(
                    "${controller.userName}",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Divider(height: 16.0 * 2),
                  Info(
                    infoKey: "First Name",
                    info: "${controller.firstName}",
                  ),
                  Info(
                    infoKey: "Last Name",
                    info: "${controller.lastName}",
                  ),
                  Info(
                    infoKey: "Address",
                    info: "${controller.address}",
                  ),
                  Info(
                    infoKey: "Gender",
                    info: "${controller.gender}",
                  ),
                  // Info(
                  //   infoKey: "Date of Birth",
                  //   info:
                  //       "${DateFormat().add_yMd().format(DateTime.parse(controller.dateofbirth.value))}",
                  // ),
                  Info(
                    infoKey: "Phone Number",
                    info: "${controller.phoneNumber}",
                  ),
                  SizedBox(height: 16),
                  Align(
                    alignment: Alignment.center,
                    child: SizedBox(
                      width: 120,
                      child: DefaultButton(
                        text: "Edit Profile",
                        press: () {
                          Navigator.pushNamed(
                              context, EditProfileScreen.routeName);
                        },
                      ),
                    ),
                  ),
                ],
              ),
            );
        },
      ),
    );
  }
}
