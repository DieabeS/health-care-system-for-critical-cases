import 'package:flutter/material.dart';
import 'body.dart';


 class ForgotPasswordScreen extends StatelessWidget {
  static String routeName = '/submit';
   @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.greenAccent,
          body: Container(
              height: double.infinity,
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                Colors.blue,
                Colors.greenAccent.withOpacity(0.8)
              ])),
              child: Body())),
    );
  }
 }
 
 

