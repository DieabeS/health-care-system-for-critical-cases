import 'package:flutter/material.dart';
import 'package:senior/components/default_button.dart';

class ForgotPasswordForm extends StatefulWidget {
  @override
  _ForgotPasswordFormState createState() => _ForgotPasswordFormState();
}

class _ForgotPasswordFormState extends State<ForgotPasswordForm> {
  @override
  Widget build(BuildContext context) {
    return Form(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(children: [
          TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.email_outlined),
                hintText: "EmailAddress",
                hintStyle: TextStyle(fontSize: 15)),
          ),
          SizedBox(height: 70),
         
          DefaultButton(
              text: 'Submit',
              press: () {
                ////////////////////api////////////
              }),
          
        ]),
      ),
    );
  }
}