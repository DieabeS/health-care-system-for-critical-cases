import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:senior/controllers/user_controller.dart';
import 'package:senior/model/complete_profile_model.dart';
import 'package:senior/model/edit_profile_model.dart';
import 'package:senior/model/search_results_model.dart';
import 'package:senior/model/sign_in_model.dart';
import 'package:senior/model/sign_up_model.dart';
import 'package:senior/model/info_note_model.dart';

class Api {
  UserController controller = Get.find<UserController>();

  Future<SignInModel> logIn(SignInRequest signInRequest) async {
    Uri url = Uri.parse('http://sepld.herokuapp.com/authentication/login');

    final response = await http.post(
      url,
      body: json.encode(signInRequest.toJson()),
      headers: {"Content-Type": "application/json"},
    );
    print(response.statusCode);
    print(response.body);

    if (response.statusCode == 200) {
      print(response.body);
      return SignInModel.fromJson((jsonDecode(response.body)));
    } else {
      throw Exception("cannot sign in");
    }
  }

  Future<SignUpModel> signup(SignUpRequest signUpRequest) async {
    Uri url = Uri.parse("http://sepld.herokuapp.com/authentication/signup");

    final response = await http.post(
      url,
      body: json.encode(signUpRequest.toJson()),
      headers: {"Content-Type": "application/json"},
    );

    print(response.statusCode);
    print(response.body);
    print(signUpRequest.toJson());

    if (response.statusCode == 200) {
      print(response.body);
      return SignUpModel.fromJson(json.decode(response.body));
    } else {
      throw Exception("cannot sign up");
    }
  }

  Future<CompleteProfileModel> completeProfile(
      CompleteProfileRequest completeProfileRequest) async {
    Uri url =
        Uri.parse('http://sepld.herokuapp.com/authentication/complete-profile');

    final response = await http.post(
      url,
      body: completeProfileRequest.toJson(),
      headers: {"Content-Type": "application/json"},
    );
    print(response.statusCode);

    if (response.statusCode == 200) {
      print(response.body);
      return CompleteProfileModel.fromJson(json.decode(response.body));
    } else {
      throw Exception("Error");
    }
  }

  Future<InfoNoteModel> infonote() async {
    Uri url = Uri.parse('http://sepld.herokuapp.com/authentication/profile/' +
        '60c24fdea621e10015e0c505');
    print(url);
    final response = await http.get(
      url,
      headers: {"Content-Type": "application/json"},
    );
    print(response.statusCode);
    if (response.statusCode == 200) {
      print(response.body);
      return InfoNoteModel.fromJson(json.decode(response.body));
    } else {
      throw Exception("error");
    }
  }

  Future<SearchResult> getResult() async {
    Uri url = Uri.parse('http://sepld.herokuapp.com/hospitals/view');
    final response = await http.get(
      url,
      headers: {"Content-Type": "application/json"},
    );
    print(response.statusCode);
    print(response.body);

    if (response.statusCode == 200) {
      print(response.body);
      return SearchResult.fromJson(json.decode(response.body));
    } else {
      throw Exception("error");
    }
  }

  Future editProfile(EditProfileRequest editProfileRequest) async {
    Uri url =
        Uri.parse('http://sepld.herokuapp.com/authentication/edit-profile');

    final response = await http.put(
      url,
      body: jsonEncode(editProfileRequest.toJson()),
      headers: {"Content-Type": "application/json"},
    );
    print(response.statusCode);

    if (response.statusCode == 200) {
      print(response.body);
      return CompleteProfileModel.fromJson(json.decode(response.body));
    } else {
      throw Exception("Error");
    }
  }
}
